import os
import sys
import psutil
import time
import platform


def open_app(doors_path):
    for p in psutil.process_iter():
        if "doors.exe" in p.name():  # 进程名区分大小写
            print("DOORS is running. Script will exit in 3s.")
            time.sleep(3)
            sys.exit()

    # print(os.path.split(doors_path))

    try:
        os.startfile(doors_path)
    except:
        print("Please check DOORS path.")
        print("#########################################")
        print("# Environment Variable Name: DOORS_PATH #")
        print("# Value: <your_path>\\doors.exe          #")
        print("#########################################")
        print("Scrip will exit in 8s.")
        time.sleep(8)
        sys.exit()

    time.sleep(1)  # 等待应用启动

    start_times = 1
    while True:
        print("========Starting DOORS %d time(s)========" % start_times)

        for p in psutil.process_iter():
            if "doors.exe" in p.name():  # 进程名区分大小写
                doors_process = p
                doors_pid = p.pid
                break

        time.sleep(1)  # 等待2s后判断应用运行状态

        threads_first_count = doors_process.num_threads()
        time.sleep(1)
        threads_recount = doors_process.num_threads()

        threads_increasing_times = 0
        loop_count = 1
        while True:
            count_difference = threads_recount - threads_first_count
            # 根据线程数差值判断是否获取license，大于等于2可能会判断错误
            if count_difference >= 2:
                threads_increasing_times = threads_increasing_times + 1

            print("Loop: %d. Thread: (%d, %d). Difference: (%d, %d)." % (loop_count,
                                                                         threads_first_count,
                                                                         threads_recount,
                                                                         count_difference,
                                                                         threads_increasing_times))

            if threads_increasing_times >= 1:
                print("Successfully open DOORS. Please login. Scrip will exit in 3s.")
                time.sleep(3)
                sys.exit()

            if loop_count >= 12:
                print("Timeout. No license. Script will kill DOORS and restart.")
                for p in psutil.process_iter():
                    if "doors.exe" in p.name():  # 进程名区分大小写
                        doors_pid = p.pid
                        # /F 可以强制结束未相应的程序
                        os.popen('taskkill.exe /F /pid:' + str(doors_pid))
                        time.sleep(1)

                print("DOORS will automaticlly restart in 3 seconds.")
                time.sleep(3)

                os.startfile(doors_path)
                break

            time.sleep(1)
            threads_recount = doors_process.num_threads()

            loop_count = loop_count + 1

        if start_times == 1000:
            print("DOORS will automaticlly restart in 3 seconds.")
            break
        start_times = start_times + 1


def is_new_release():
    release = platform.platform.release()
    try:
        if float(release) >= 8:
            return True
    except ValueError:
        if (release == "post2008Server"):
            return True
    return False


if __name__ == "__main__":
    print("#########################################")
    print("# Author: Wu Mengze                     #")
    print("# Email: wu_mengze@careri.com           #")
    print("#########################################")

    try:
        doors_path = os.environ['DOORS_PATH']
        print("DOORS path: ", doors_path)
    except:
        print("Please check DOORS path.")
        print("#########################################")
        print("# Environment Variable Name: DOORS_PATH #")
        print("# Value: <your_path>\\doors.exe          #")
        print("#########################################")
        print("Scrip will exit in 8s.")

        time.sleep(8)
        sys.exit()

    if not (os.path.isfile(doors_path) and os.path.split(doors_path)[-1] == "doors.exe"):
        print("Please check DOORS path.")
        print("#########################################")
        print("# Environment Variable Name: DOORS_PATH #")
        print("# Value: <your_path>\\doors.exe          #")
        print("#########################################")
        print("Scrip will exit in 8s.")
        time.sleep(8)
        sys.exit()

    open_app(doors_path)
